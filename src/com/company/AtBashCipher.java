package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AtBashCipher implements Cipher {

    String alphabetUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String reverseAlphabetUpperCase = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    String alphabetLowerCase = "abcdefghijklmnopqrstuvwxyz";
    String reverseAlphabetLowerCase = "zyxwvutsrqponmlkjihgfedcba";
    @Override
    public String decode(String message) {
        return encode(message);
    }

    @Override
    public String encode(String message) {
        String encodeMessage = "";
        for(char letter: message.toCharArray()){
            if(Character.isUpperCase(letter)){
                int letterPos = alphabetUpperCase.indexOf(letter);
                char encodeLetter = reverseAlphabetUpperCase.charAt(letterPos);
                encodeMessage += encodeLetter;
            }
            else if(Character.isLowerCase(letter)){
                int letterPos = alphabetLowerCase.indexOf(letter);
                char encodeLetter = reverseAlphabetLowerCase.charAt(letterPos);
                encodeMessage += encodeLetter;
            }
            else if(letter == ' '){
                encodeMessage +=  " ";
            }
            else{
                encodeMessage = "Wiadomosc moze zawierac tylko litery";
            }

        }
        return encodeMessage;
    }
}
